<a id="pt-spec.txt-3.2"></a>

# Pluggable Transport Configuration Environment Variables {#envvars}

All Pluggable Transport proxy instances are configured by their
parent process at launch time via a set of well defined
environment variables.

The "TOR_PT\_" prefix is used for namespacing reasons and does not
indicate any relations to Tor, except for the origins of this
specification.

<a id="pt-spec.txt-3.2.1"></a>

## Common Environment Variables {#common}

When launching either a client or server Pluggable Transport proxy,
the following common environment variables MUST be set.

"TOR_PT_MANAGED_TRANSPORT_VER"

Specifies the versions of the Pluggable Transport specification
the parent process supports, delimited by commas.  All PTs MUST
accept any well-formed list, as long as a compatible version is
present.

Valid versions MUST consist entirely of non-whitespace,
non-comma printable ASCII characters.

The version of the Pluggable Transport specification as of this
document is "1".

Example:

TOR_PT_MANAGED_TRANSPORT_VER=1,1a,2b,this_is_a_valid_ver

"TOR_PT_STATE_LOCATION"

Specifies an absolute path to a directory where the PT is
allowed to store state that will be persisted across
invocations.  The directory is not required to exist when
the PT is launched, however PT implementations SHOULD be
able to create it as required.

PTs MUST only store files in the path provided, and MUST NOT
create or modify files elsewhere on the system.

Example:

TOR_PT_STATE_LOCATION=/var/lib/tor/pt_state/

"TOR_PT_EXIT_ON_STDIN_CLOSE"

Specifies that the parent process will close the PT proxy's
standard input (stdin) stream to indicate that the PT proxy
should gracefully exit.

PTs MUST NOT treat a closed stdin as a signal to terminate
unless this environment variable is set to "1".

PTs SHOULD treat stdin being closed as a signal to gracefully
terminate if this environment variable is set to "1".

Example:

TOR_PT_EXIT_ON_STDIN_CLOSE=1

"TOR_PT_OUTBOUND_BIND_ADDRESS_V4"

Specifies an IPv4 IP address that the PT proxy SHOULD use as source address for
outgoing IPv4 IP packets. This feature allows people with multiple network
interfaces to specify explicitly which interface they prefer the PT proxy to
use.

If this value is unset or empty, the PT proxy MUST use the default source
address for outgoing connections.

This setting MUST be ignored for connections to
loopback addresses (127.0.0.0/8).

Example:

TOR_PT_OUTBOUND_BIND_ADDRESS_V4=203.0.113.4

"TOR_PT_OUTBOUND_BIND_ADDRESS_V6"

Specifies an IPv6 IP address that the PT proxy SHOULD use as source address for
outgoing IPv6 IP packets. This feature allows people with multiple network
interfaces to specify explicitly which interface they prefer the PT proxy to
use.

If this value is unset or empty, the PT proxy MUST use the default source
address for outgoing connections.

This setting MUST be ignored for connections to the loopback address (\[::1\]).

IPv6 addresses MUST always be wrapped in square brackets.

Example::

TOR_PT_OUTBOUND_BIND_ADDRESS_V6=\[2001:db8::4\]

<a id="pt-spec.txt-3.2.2"></a>

## Pluggable Transport Client Environment Variables {#client}

Client-side Pluggable Transport forward proxies are configured
via the following environment variables.

"TOR_PT_CLIENT_TRANSPORTS"

Specifies the PT protocols the client proxy should initialize,
as a comma separated list of PT names.

PTs SHOULD ignore PT names that it does not recognize.

Parent processes MUST set this environment variable when
launching a client-side PT proxy instance.

Example:

TOR_PT_CLIENT_TRANSPORTS=obfs2,obfs3,obfs4

"TOR_PT_PROXY"

Specifies an upstream proxy that the PT MUST use when making
outgoing network connections.  It is a URI \[RFC3986\] of the
format:

`<proxy_type>://[<user_name>[:<password>][@]<ip>:<port>`.

The "TOR_PT_PROXY" environment variable is OPTIONAL and
MUST be omitted if there is no need to connect via an
upstream proxy.

Examples:

```text
           TOR_PT_PROXY=socks5://tor:test1234@198.51.100.1:8000
           TOR_PT_PROXY=socks4a://198.51.100.2:8001
           TOR_PT_PROXY=http://198.51.100.3:443
```

<a id="pt-spec.txt-3.2.3"></a>

## Pluggable Transport Server Environment Variables {#server}

Server-side Pluggable Transport reverse proxies are configured
via the following environment variables.

"TOR_PT_SERVER_TRANSPORTS"

Specifies the PT protocols the server proxy should initialize,
as a comma separated list of PT names.

PTs SHOULD ignore PT names that it does not recognize.

Parent processes MUST set this environment variable when
launching a server-side PT reverse proxy instance.

Example:

TOR_PT_SERVER_TRANSPORTS=obfs3,scramblesuit

"TOR_PT_SERVER_TRANSPORT_OPTIONS"

Specifies per-PT protocol configuration directives, as a
semicolon-separated list of `<key>:<value>` pairs, where `<key>`
is a PT name and `<value>` is a k=v string value with options
that are to be passed to the transport.

Colons, semicolons, and backslashes MUST be
escaped with a backslash.

If there are no arguments that need to be passed to any of
PT transport protocols, "TOR_PT_SERVER_TRANSPORT_OPTIONS"
MAY be omitted.

Example:

TOR_PT_SERVER_TRANSPORT_OPTIONS=scramblesuit:key=banana;automata:rule=110;automata:depth=3

```text
         Will pass to 'scramblesuit' the parameter 'key=banana' and to
         'automata' the arguments 'rule=110' and 'depth=3'.

     "TOR_PT_SERVER_BINDADDR"
```

A comma separated list of `<key>-<value>` pairs, where `<key>` is
a PT name and `<value>` is the `<address>:<port>` on which it
should listen for incoming client connections.

The keys holding transport names MUST be in the same order as
they appear in "TOR_PT_SERVER_TRANSPORTS".

The `<address>` MAY be a locally scoped address as long as port
forwarding is done externally.

The `<address>:<port>` combination MUST be an IP address
supported by `bind()`, and MUST NOT be a host name.

Applications MUST NOT set more than one `<address>:<port>` pair
per PT name.

If there is no specific `<address>:<port>` combination to be
configured for any transports, "TOR_PT_SERVER_BINDADDR" MAY
be omitted.

Example:

TOR_PT_SERVER_BINDADDR=obfs3-198.51.100.1:1984,scramblesuit-127.0.0.1:4891

"TOR_PT_ORPORT"

Specifies the destination that the PT reverse proxy should forward
traffic to after transforming it as appropriate, as an
`<address>:<port>`.

Connections to the destination specified via "TOR_PT_ORPORT"
MUST only contain application payload.  If the parent process
requires the actual source IP address of client connections
(or other metadata), it should set "TOR_PT_EXTENDED_SERVER_PORT"
instead.

Example:

TOR_PT_ORPORT=127.0.0.1:9001

"TOR_PT_EXTENDED_SERVER_PORT"

Specifies the destination that the PT reverse proxy should
forward traffic to, via the Extended ORPort protocol \[EXTORPORT\]
as an `<address>:<port>`.

The Extended ORPort protocol allows the PT reverse proxy to
communicate per-connection metadata such as the PT name and
client IP address/port to the parent process.

If the parent process does not support the ExtORPort protocol,
it MUST set "TOR_PT_EXTENDED_SERVER_PORT" to an empty string.

Example:

TOR_PT_EXTENDED_SERVER_PORT=127.0.0.1:4200

"TOR_PT_AUTH_COOKIE_FILE"

Specifies an absolute filesystem path to the Extended ORPort
authentication cookie, required to communicate with the
Extended ORPort specified via "TOR_PT_EXTENDED_SERVER_PORT".

If the parent process is not using the ExtORPort protocol for
incoming traffic, "TOR_PT_AUTH_COOKIE_FILE" MUST be omitted.

Example:

TOR_PT_AUTH_COOKIE_FILE=/var/lib/tor/extended_orport_auth_cookie
